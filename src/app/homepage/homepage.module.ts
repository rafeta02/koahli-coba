import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomepagePageRoutingModule } from './homepage-routing.module';

import { HomepagePage } from './homepage.page';
import { HomePageModule } from '../home/home.module';
import { LoginPageModule } from '../auth/login/login.module';

import { SuperTabsModule } from '@ionic-super-tabs/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomepagePageRoutingModule,
    HomePageModule,
    LoginPageModule,
    SuperTabsModule.forRoot(),
  ],
  declarations: [HomepagePage]
})
export class HomepagePageModule {}
