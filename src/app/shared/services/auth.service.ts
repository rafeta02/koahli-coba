import { tap, switchMap, map, take } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { Platform } from '@ionic/angular';

const helper = new JwtHelperService();
const TOKEN_KEY = 'TOKEN';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: Observable<any>;
  private userData = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private plt: Platform
  ) {}

  loadStoredToken() {
    const platformObs = from(this.plt.ready());

    this.user = platformObs.pipe(
      switchMap(() => {
        return from(this.storage.get(TOKEN_KEY));
      }),
      map(token => {
        if (token) {
          const decoded = helper.decodeToken(token);
          this.userData.next(decoded);
          return true;
        } else {
          return null;
        }
      })
    );
  }

  getUser() {
    return this.userData.getValue();
  }

  login(email: string, password: string) {
    return this.http
      .post<any>(`${environment.API_URL}/user/login`, {
        email,
        password
      })
      .pipe(
        take(1),
        map(respon => {
          return respon.data;
        }),
        switchMap(data => {
          const decoded = helper.decodeToken(data.token);
          this.userData.next(decoded);
          const storageObs = from(this.storage.set(TOKEN_KEY, data.token));
          return storageObs;
        })
      );
  }

  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      // after logout
      this.userData.next(null);
    });
  }

  register(
    firstName: string,
    lastName: string,
    email: string,
    password: string
  ) {
    return this.http.post<any>(`${environment.API_URL}/user/register`, {
      first_name: firstName,
      last_name: lastName,
      email,
      password
    });
  }

  tokenCheck() {
    return this.http.get<any>(`${environment.API_URL}/user/token/check`).pipe(
      map(respon => {
        return respon.data;
      })
    );
  }
}
