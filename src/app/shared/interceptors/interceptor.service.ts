import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { tap, switchMap, map, catchError } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { from, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(
    private storage: Storage,
    private alertController: AlertController
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    return from(this.storage.get('TOKEN'))
      .pipe(
        switchMap(token => {
          if (token) {
            request = request.clone({
              setHeaders: {
                'x-access-token': token
              }
            });
          }

          if (!request.headers.has('Content-Type')) {
            request = request.clone({
              setHeaders: {
                'Content-Type': 'application/json'
              }
            });
          }

          // request = request.clone({
          //   headers: request.headers.set('Accept', 'application/json')
          // });

          return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
              if (event instanceof HttpResponse) {
                // do nothing for now
              }
              return event;
            }),
            catchError((error: HttpErrorResponse) => {
              const status = error.status;

              console.log('error', error);

              const reason = error && error.error.reason ? error.error.reason : '';

              this.presentAlert(status, reason);
              return throwError(error);
            })
          );
        })
      );

  }

  async presentAlert(status, reason) {
    const alert = await this.alertController.create({
      header: status + ' Error',
      subHeader: 'Subtitle',
      message: reason,
      buttons: ['OK']
    });

    await alert.present();
  }
}
