import { Component } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  id: any;
  name: any;

  constructor(private navParams: NavParams) {
    this.id = navParams.get('id');
    this.name = navParams.get('name');
  }


}
