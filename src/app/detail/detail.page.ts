import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonList, IonItem } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  categories = ['Overview', 'Description', 'Package Price', 'Cancel Policy', 'FAQ', 'About the Seller',
  'Reviews'];

  @ViewChild(IonList, { read: ElementRef, static: true }) list: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  onCategoryChange(category) {
    console.log(category);
  }

  scrollListVisible(index) {
    const arr = this.list.nativeElement.children;
    const item = arr[index];
    item.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }

  check(e) {
    console.log('check', e);
  }

  onElementScroll(e) {
    console.log('event', e);
  }

  logScrollStart() {
    console.log('aaaa');
  }

}
