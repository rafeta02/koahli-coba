import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  form: FormGroup;

  constructor(
    private auth: AuthService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      firstName: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      lastName: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(6)]
      }),
      passwordConfirm: new FormControl(null, {
        updateOn: 'change',
        validators: [Validators.required, Validators.minLength(6)]
      })
    }, {
      validators: this.checkPassword
    });
  }

  checkPassword(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.passwordConfirm.value;

    if (pass !== confirmPass) {
      group.controls.passwordConfirm.setErrors({ MustMatch: true });
    }

    return pass === confirmPass ? null : { notSame: true };
  }

  register() {
    if (this.form.invalid) {
      return;
    }

    this.loadingCtrl.create({
      message: 'Sign in...'
    })
      .then(loadingEl => {
        loadingEl.present();
        this.auth.register(this.form.value.firstName, this.form.value.lastName, this.form.value.email, this.form.value.password).subscribe(
          respon => {
            console.log(respon);
            loadingEl.dismiss();
          }
        );
      });
  }

}
