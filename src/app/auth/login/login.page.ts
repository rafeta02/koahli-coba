import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  form: FormGroup;
  username: string;

  constructor(
    private auth: AuthService,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(6)]
      })
    });
  }

  signIn() {
    if (!this.form.valid) {
      return;
    }

    this.loadingCtrl
      .create({
        message: 'Logging in...'
      })
      .then(loadingEl => {
        loadingEl.present();
        this.auth
          .login(this.form.value.email, this.form.value.password)
          .subscribe(response => {
            console.log(response);
            this.auth.tokenCheck().subscribe(check => {
              console.log('check', check);
            });
            loadingEl.dismiss();
          });
      });
  }
}
