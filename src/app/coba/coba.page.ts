import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonList, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-coba',
  templateUrl: './coba.page.html',
  styleUrls: ['./coba.page.scss'],
})
export class CobaPage implements OnInit {

  @ViewChild(IonList, { read: ElementRef, static: true }) list: ElementRef;
  @ViewChild(IonContent, { static: true}) content: IonContent;

  arr = [];

  block = 'center';
  behaviour = 'smooth';
  scrollTo = null;
  offsetTop = 0;

  constructor() {
    for (let val = 0; val < 100; val++) {
      this.arr.push(`Element - ${val}`);
    }
  }

  ngOnInit() {
  }

  scrollListVisible() {
    const arr = this.list.nativeElement.children;
    const item = arr[this.scrollTo];
    item.scrollIntoView({ behavior: this.behaviour, block: this.block });
  }

  scrollBottom() {
    this.content.scrollToBottom();
  }

  scrollTop() {
    this.content.scrollToTop();
  }

  onScroll(e) {
    this.offsetTop = e.detail.scrollTop;
  }
}
